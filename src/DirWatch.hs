module DirWatch
  ( getDevEvent
  , DevEvent(..)
  )
where

import           System.Directory               ( listDirectory )
import           Data.List
import           Data.List.NonEmpty
import           System.FilePath                ( (</>) )

data DevEvent = NothingNew | NewDevices DevicePaths
  deriving (Show)

type DevicePaths = NonEmpty FilePath
type DirContents = [FilePath]

-- Gets the absolute file paths inside a dir
listDirectoryAbsolute :: FilePath -> IO [FilePath]
listDirectoryAbsolute dir = do
  relativePaths <- listDirectory dir
  return $ fmap (dir </>) relativePaths

getDevEvent :: Maybe DirContents -> FilePath -> IO (DevEvent, DirContents)
getDevEvent oldFilesMaybe dirPath = do
  currentFiles <- listDirectoryAbsolute dirPath
  let event = getDevEvent' oldFilesMaybe currentFiles
  return (event, currentFiles)

getDevEvent' :: Maybe DirContents -> DirContents -> DevEvent
getDevEvent' oldFilesMaybe currentFiles =
  -- The difference between the files in the directory from the
  -- previous cycle and the current cycle
  let newFiles = case oldFilesMaybe of
        (Just oldFiles) -> currentFiles \\ oldFiles
        Nothing         -> currentFiles
  -- Either there are no new devices or there's a non-empty
  -- list of new devices
  in  maybe NothingNew NewDevices (nonEmpty newFiles)
