module Mounter
  ( mount
  )
where
import           System.Process                 ( readProcessWithExitCode )
import           System.Exit                    ( ExitCode )
import           Data.List.NonEmpty

type StdOut = String
type StdErr = String

mount :: FilePath -> IO (ExitCode, StdOut, StdErr)
mount devicePath =
  callCmd $ "udisksctl" :| ["mount", "--block-device", devicePath]

callCmd :: NonEmpty String -> IO (ExitCode, StdOut, StdErr)
callCmd (cmd :| args) = readProcessWithExitCode cmd args stdIn
  where stdIn = ""
