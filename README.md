# mountie
Mounts the file system of a device, once it's plugged in.

Similar to [udiskie](https://github.com/coldfix/udiskie) and [udevil](https://ignorantguru.github.io/udevil/), but with fewer features.

# Prerequisites
mountie uses the command `udisksctl` to mount devices. On Arch Linux, this command is part of package `udisks2`.

# Running mountie
Clone the repository, then use [stack](https://docs.haskellstack.org/en/stable/README/) to run mountie:

    stack run
