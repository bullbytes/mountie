{-# Language OverloadedStrings, ScopedTypeVariables #-}
module Main where

import           DirWatch                       ( getDevEvent
                                                , DevEvent(..)
                                                )
import           Mounter                        ( mount )
import           Control.Concurrent             ( threadDelay )
import           Control.Monad                  ( (>=>) )

main :: IO ()
main = do
  let watchedDir = "/dev/disk/by-uuid/"
  putStrLn $ "Mountie started, looking for devices in " ++ watchedDir
  loop watchedDir

loop :: FilePath -> IO ()
loop dir = go dir Nothing
 where
  go dir dirContentsMaybe = do
    (event, dirContents) <- getDevEvent dirContentsMaybe dir
    case event of
      (NewDevices devicePaths) -> do
        putStrLn $ "Let's mount these new devices: " ++ show devicePaths
        mapM_
          -- Mount each device, then print the result of the mounting
          (mount >=> \(exitCode, out, err) ->
            putStrLn
              $  "Exit code: "
              ++ show exitCode
              ++ "\nOut: "
              ++ out
              ++ "\nErr: "
              ++ err
          )
          devicePaths
      _ -> return ()
    wait
    go dir $ Just dirContents

wait = threadDelay (2 * 10 ^ 6)
